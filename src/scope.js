/* jshint globalstrict: true */

'use strict'

function Scope() {
	this.$$watchers = [];	// $$ signifies private to Angular framework and should not be called from application code.
                         
    this.$$children = [];
    this.$$lastDirtyWatch = null;
}

// This is to initialize 'watcher.last'. Reason for this is because functions in JS
// are reference values. They are not considered equal to anything but themselves. 
function initWatchVal() {}	

// Chapter 2: Scopes
Scope.prototype.$new = function() {
  var ChildScope = function() {}
  ChildScope.prototype = Object.create(this);
  var child = new ChildScope();
  child.$$watchers = [];
  child.$$children = [];
  this.$$children.push(child); // Parent's children list.
  return child;
};

Scope.prototype.$$everyScope = function() {
    if(fn(this)) {
        return this.$$children.every(function(child) {
            return child.$$everyScope(fn);
        });        
    } else {
        return false;
    }
}

// Chapter 1: Digest cycle
// $watch will just store references to watch and listener functions.
Scope.prototype.$watch = function(watchFn, listenerFn, valueEq) {
	var watcher = {
		watchFn: watchFn,
		listenerFn: listenerFn || function() { },
		last: initWatchVal,
        valueEq: !!valueEq    // Coerse to boolean by negating twice. If user doesn't pass in valueEq, it will be undefined which is equal to 'false'
                              // This is to perform a value check of of each property in an object.
	};
	
	this.$$watchers.push(watcher);
    this.$$lastDirtyWatch = null;
};

Scope.prototype.$$areEqual = function(newValue, oldValue, valueEq) {
    if(valueEq){
        return _.isEqual(newValue, oldValue);
    }
    else {
        return newValue == oldValue || 
               (
                   // Manual handling of NaN because in JS NaN != NaN
                   typeof newValue === 'number' && typeof oldValue  === 'number' && 
                    (isNaN(newValue) && isNaN(oldValue))
               );
    }    
};

// $digest will invoke all listener functions for each watcher.
Scope.prototype.$$digestOnce = function() {
	var self = this;	// JS Binding problem: http://alistapart.com/article/getoutbindingsituations
						// this context is scope in scope_spec.js because this is called using scope.$digest. The context is determined from the call site.
                        
	var newValue, oldValue, dirty;
	
	_.forEach(this.$$watchers, function(watcher) {
		newValue = watcher.watchFn(self);	// This will run whatever is implemented in watchFn in scope_spec.js
		oldValue = watcher.last;		// On first run the value is a reference to function initWatchVal()
		
		// First run, JS will compare that undefined != initWatchVal
		if(!self.$$areEqual(newValue, oldValue, watcher.valueEq))	
		{
            self.$$lastDirtyWatch = watcher;
			watcher.last = watcher.valueEq ? _.cloneDeep(newValue) : newValue;
			watcher.listenerFn(newValue, oldValue === initWatchVal ? newValue : oldValue, self);
            dirty = true;   // Notice that this causes $digest to run at least twice as long as a watched value changes. 
		}
        // If we encounter a clean watch which was dirty, break out.
        else if(self.$$lastDirtyWatch === watcher) {            
            // Notice that self.$$lastDirtyWatch contains the 'last' dirty watch in the previous (very first) digest.
            // So this condition will only run after the end of the first digest.
            return false;   // This causes lodash _forEach to short circuit and exit.
        }
	});
    
    return dirty;
}

Scope.prototype.$digest = function() {
    var ttl = 10;
    var dirty;
    this.$$lastDirtyWatch = null;
    
    do {
        dirty = this.$$digestOnce();
        
        if(dirty && !ttl--) // ttl is only false when ttl == 0. No more, no less.
        {
            throw "10 digest iterations reached";
        }
    } while(dirty);
};

Scope.prototype.$eval = function(expr, locals) {
   return expr(this, locals); 
};

// Apply changes by external libraries that aren't 'Angular aware'.
Scope.prototype.$apply = function(expr) {
  try {
      return this.$eval(expr);
  }
  finally {
      this.$digest();
  }
};

// Doesn't hand control over to browser and runs strictly in the same $digest.
// +ve is that browser doesn't render a DOM that will immediately get re-rendered.
Scope.prototype.$evalAsync = function() {
    // Pg 60 - end of chapter
};